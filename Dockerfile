# Build stage
FROM python:3.11-slim as builder

# Set working directory
WORKDIR /app

# Copy only necessary files for building docs
COPY mkdocs.yml .
COPY docs/ ./docs/
COPY requirements.txt .
COPY brynq_sdk_*/ ./brynq_sdk_*/

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Build the documentation
RUN mkdocs build

# Serve stage
FROM nginx:alpine

# Copy the built docs from previous stage
COPY --from=builder /app/site /usr/share/nginx/html

# Create custom nginx config
RUN echo 'server { \n\
    listen 8910; \n\
    server_name localhost; \n\
    location / { \n\
        root /usr/share/nginx/html; \n\
        index index.html; \n\
    } \n\
}' > /etc/nginx/conf.d/default.conf

# Expose port 8910
EXPOSE 8910

# Command to run nginx in foreground
CMD ["nginx", "-g", "daemon off;"]
