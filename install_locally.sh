read -p "Enter Customer: "  customer
read -p "Enter package name (without brynq_sdk_ prefix): "  package
py -m pip install build
py -m build -s --outdir C:/data_analytics/$customer brynq_sdk_$package
rm -rf ./brynq_sdk_$package/brynq_sdk_$package.egg-info
cd C:/data_analytics/$customer
docker build -t $customer .
read