import requests
import pandas as pd


class Payroll:
    def __init__(self, factorial):
        self.factorial = factorial
        self.base_endpoint = '/resources/payroll/supplements'

    def get(self,
            to_date: str = None, # YYYY-MM-DD
            from_date: str = None, # YYYY-MM-DD
            legal_entity_ids: list = None,
            policy_period_ids: list = None,
            compensation_id: int = None,
            ids: list = None,
            employee_ids: list[int] = None) -> pd.DataFrame:

        # Use locals() to get all function parameters as a dictionary. Do not move this down since it will include all variables in function scope
        func_params = list(locals().items())
        params = {}
        for param, value in func_params:
            if param != 'self' and value is not None:
                params[param] = value

        has_next_page = True
        result_data = pd.DataFrame()
        while has_next_page:
            response = self.factorial.session.get(url=f'{self.factorial.base_url}{self.base_endpoint}',
                                                  params=params)
            response.raise_for_status()
            response_data = response.json()
            result_data = pd.concat([result_data, pd.DataFrame(response_data['data'])])
            has_next_page = response_data['meta']['has_next_page']
            if has_next_page:
                params['after_id'] = response_data['meta']['end_cursor']

        return result_data

    def update(self, supplement_id: str, data: dict) -> requests.Response:
        response = self.factorial.session.put(url=f'{self.factorial.base_url}{self.base_endpoint}/{supplement_id}',
                                              json=data)
        response.raise_for_status()
        return response

    def delete(self, supplement_id: str) -> requests.Response:
        response = self.factorial.session.delete(url=f'{self.factorial.base_url}{self.base_endpoint}/{supplement_id}')
        response.raise_for_status()
        return response

    def create(self, data: dict) -> requests.Response:
        raise NotImplementedError
