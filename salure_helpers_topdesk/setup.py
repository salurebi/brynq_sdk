from setuptools import setup


setup(
    name='salure_helpers_topdesk',
    version='0.0.2',
    description='Topdesk wrapper from Salure',
    long_description='Topdesk wrapper from Salure',
    author='D&A Salure',
    author_email='support@salureconnnect.com',
    packages=["salure_helpers.topdesk"],
    license='Salure License',
    install_requires=[
        'salure-helpers-salureconnect>=1',
        'pandas>=1,<=1.35',
        'requests>=2,<=3'
    ],
    zip_safe=False,
)