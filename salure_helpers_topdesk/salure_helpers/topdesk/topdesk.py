import urllib.parse
import warnings
import requests
import json
from typing import Union, List
from salure_helpers.salureconnect import SalureConnect
import pandas as pd
import os

class Topdesk(SalureConnect):
    """
    This class is meant to be a simple wrapper around the Topdesk API. In order to start using it, authorize your application is Salureconnect.
    You will need to provide a username and password for the authorization, which can be set up in Salureconnect and referred as Label.
    """
    def __init__(self, label: Union[str, List]):
        super().__init__()
        self.credentials = self.get_system_credential(system='topdesk', label=label)
        self.base_url = self.credential['base_url']
        self.auth = (self.credential['username'], self.credential['password'])

    def topdesk_conn(self, endpoint, method='POST', data=None):
        """
        Define a request method and call the topdesk api
        :return: (n.a.)
        """
        # Set the headers for the request
        headers = {
            'Accept': '*/*',
            'Content-Type': 'application/json'
        }

        # Make the API request
        # call get request to topdesk api
        if method == 'POST':
            response = requests.post(
                f'{self.base_url}{endpoint}',
                auth=self.auth,
                headers=headers,
                json=data
            )
        elif method == 'GET':
            response = requests.get(
                f'{self.base_url}{endpoint}',
                auth=self.auth,
                headers=headers
            )
        elif method == 'PATCH':
            headers = {
                'Accept': '*/*',
                'Content-Type': r'application/json-patch+json'
            }
            response = requests.patch(
                f'{self.base_url}{endpoint}',
                auth=self.auth,
                headers=headers,
                json=data
            )
        else:
            raise requests.RequestException(f'The method {method} is not supported')
        return response

    def get_topdesk_templates(self, template_names: Union[str, List]):
        """
        This function gets the template from Topdesk
        The templates are used to create a change request
        :return: dict of templates with id and name
        """
        # Set the API endpoint and any required parameters
        endpoint = '/applicableChangeTemplates' # This might differ per customer
        template_results = self.topdesk_conn(endpoint=endpoint, method='GET')
        if isinstance(template_names, str): template_names = [template_names]
        templates = dict()
        for template in template_results['results']:
            if template['number'] in template_names:
                templates[template['number']] = (template['id'], template['briefDescription'])
        return templates

    def get_topdesk_manager_id(self, manager_list):
        """
        This function gets the manager ids from Topdesk person endpoint
        :return: dict of templates with id and name
        """
        # Set the API endpoint and any required parameters
        endpoint = '/persons?query=isManager==True;archived==False&page_size=500'
        person_results = self.topdesk_conn(endpoint=endpoint, method='GET')
        managers = dict()
        for person in person_results:
            employe_number = person['employeeNumber']
            if employe_number in manager_list:
                managers[employe_number] = person['id']
        return managers

    def get_topdesk_activity_id(self, ticket_number, activity_name):
        """
        This function gets the activity ids from Topdesk operatorChangeActivities endpoint
        Look at the activity name in the ticket and get the id
        :return: dict of templates with id and name
        """
        # Set the API endpoint and any required parameters
        endpoint = f"/operatorChangeActivities?query=number!='{ticket_number}'"
        activity_response = self.topdesk_conn(endpoint=endpoint, method='GET')

        for activity in activity_response['results']:
            if activity['change']['number'] == ticket_number and activity['briefDescription'] == activity_name:
                return activity['id']
        return None

    def patch_activity_plannedFinalDate(self, activity_id, planned_final_date):
        """
        Patch the activity with the plannedFinalDate
        :param activity_id:
        :param plannedFinalDate:
        :return:
        """
        data = [
            {
                "op": "replace",
                "path": "/plannedFinalDate",
                "value": f"{planned_final_date.strftime(self.datetime_format)}T12:00:00+0000"
            }
        ]
        response = self.topdesk_conn(data=data, endpoint=f'/operatorChangeActivities/{activity_id}', method='PATCH')
        return response

    def loop_60sec_for_activity(self, ticket_number, activity_name):
        """
        This function loops for 60 seconds and checks if the activity is created
        :return: id of the activity, which will be used to update(patch) the plannedFinalDate
        """
        activity_id = None
        loop_counter = 1
        while not activity_id and loop_counter < 5:
            time.sleep(15)
            activity_id = self.get_topdesk_activity_id(ticket_number, activity_name)
            loop_counter += 1
        if activity_id:
            self.write_execution_log(f"Activity: Number {ticket_number} - Loop counter {loop_counter}", "INFO")
        else:
            self.write_execution_log(f"Activity: Number {ticket_number} - Loop counter {loop_counter}", "ERROR")
        return activity_id