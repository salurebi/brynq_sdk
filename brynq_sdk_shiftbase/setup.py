from setuptools import setup, find_namespace_packages

setup(
    name='brynq_sdk_shiftbase',
    version='2.0.1',
    description='Shiftbase wrapper from BrynqQ',
    long_description='Shiftbase wrapper from BrynQ',
    author='D&A BrynQ',
    author_email='support@brynq.com',
    packages=find_namespace_packages(include=['brynq_sdk*']),
    license='BrynQ License',
    install_requires=[
        'brynq_sdk_brynq>=2',
        'pandas>=1,<=3',
        'requests>=2,<=3'
    ],
    zip_safe=False,
)