from setuptools import setup, find_namespace_packages

setup(
    name='brynq_sdk_nmbrs',
    version='1.1.1',
    description='Nmbrs wrapper from BrynQ',
    long_description='Nmbrs wrapper from BrynQ',
    author='BrynQ',
    author_email='support@brynq.com',
    packages=find_namespace_packages(include=['brynq_sdk*']),
    license='BrynQ License',
    install_requires=[
        'brynq-sdk-brynq>=2',
        'pandas>=2.2.0,<3.0.0',
        'zeep>=4.0.0,<5.0.0',
        'brynq-sdk-functions>=2.0.5'
    ],
    zip_safe=False,
)
