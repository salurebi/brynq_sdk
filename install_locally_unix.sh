#!/bin/bash

# Check if -docker parameter is passed
use_docker=false
if [ "$1" == "-docker" ]; then
    use_docker=true
fi

# Get current user's home directory
current_user=$(whoami)
default_dir="/Users/$current_user/data_analytics"

# Prompt for data_analytics directory with default value
read -p "Enter the home directory of your data_analytics folder [if nothing entered, this directory will be used: $default_dir]: " data_analytics_dir
# Use default if empty
data_analytics_dir=${data_analytics_dir:-$default_dir}

read -p "Enter customer folder: "  customer
read -p "Enter package name (without brynq_sdk_ prefix): "  package

# Create and activate a virtual environment
python3 -m venv venv
source venv/bin/activate

# Install build package if not already installed and then build the package
pip3 install build
python -m build -s --outdir $data_analytics_dir/$customer brynq_sdk_$package
# Remove the egg-info directory to clean up
rm -rf ./brynq_sdk_$package/brynq_sdk_$package.egg-info

# Deactivate the brynq_sdk virtual environment before proceeding
deactivate

# Change directory to the customer's folder
cd $data_analytics_dir/$customer

if [ "$use_docker" = true ]; then
    # Build the docker image
    echo "Building Docker image..."
    docker build -t $customer .
else
    # Install the package in the existing .venv
    echo "Installing package in existing .venv environment..."
    
    # Check if .venv exists
    if [ ! -d ".venv" ]; then
        echo "Error: .venv directory not found in $data_analytics_dir/$customer"
        echo "Please create a virtual environment first or use the -docker option"
        exit 1
    fi
    
    # Activate the customer's virtual environment
    source .venv/bin/activate
    
    # Install the package
    pip install --force-reinstall brynq_sdk_$package-*.tar.gz
    
    # Deactivate the customer's virtual environment
    deactivate
fi

echo "Installation complete!"
read