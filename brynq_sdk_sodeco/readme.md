README: Converting PFX to PEM Using KeyStore Explorer
Introduction
This guide explains how to convert a .pfx file (PKCS #12 format) into PEM format using KeyStore Explorer. PEM is a text-based certificate format that is widely supported and compatible with platforms requiring separate private keys and certificates.

Why Convert to PEM?
PEM format:

Stores the certificate and private key as text, making it human-readable and easier to work with.
Is widely compatible with libraries such as requests in Python, which often require the certificate and private key as separate files.
Prerequisites
Download and install KeyStore Explorer: https://keystore-explorer.org
Have your .pfx file and its password (if applicable).
Steps to Convert PFX to PEM

1. Open the PFX File
   Launch KeyStore Explorer.
   Click File > Open and select your .pfx file.
   Enter the password for the .pfx file when prompted.
2. Export the Private Key
   Select the private key from the list.
   Click Tools > Export Key Pair.
   In the export dialog:
   File Format: Select PEM for the private key.
   File Name: Specify the output filename (e.g., key.pem).
   Password: Optionally protect the private key with a password, or leave blank if no password is required.
3. Export the Certificate Chain
   Select the certificate chain associated with your private key.
   Click Tools > Export Certificates.
   In the export dialog:
   Export Length: Choose "Entire Chain" to include all certificates (leaf, intermediate, and root).
   Export Format: Select X.509.
   PEM: Ensure the "PEM" checkbox is checked.
   File Name: Specify the output filename (e.g., cert.pem).