from typing import Optional, List
from xxlimited import Str
from pydantic import BaseModel, Field
from enum import Enum
from datetime import datetime


class DocumentTypeEnum(str, Enum):
    """Document type codes defined in Prisma"""
    ASR_INFORMATION_SHEET = "ASR_informationSheet"
    ASR_REINSTATEMENT = "ASR_reinstatement"
    ASR_C32_EMPLOYER_CONTROL_CARD = "ASR_C32employerControlCard"
    ASR_C131B = "ASR_C131B"
    ASR_TEMPORARY_UNEMPLOYMENT = "ASR_TemporaryUnemployment"
    ASR_C32_EMPLOYER_BENEFIT_REQUEST = "ASR_C32employerBenefitRequest"
    ASR_C131A = "ASR_C131A"
    ASR_FIRST_DAY_OF_UNEMPLOYMENT = "ASR_FirstDayOfUnemployment"
    ASR_PROGRESSIVE_REINSTATEMENT = "ASR_progressiveReinstatement"
    SALARY_SLIP = "SalarySlip"
    ORDER_LIST_PAYMENTS = "OrderListPayments"
    SUMMARY_PAYMENT = "SummaryPayment"
    COINS_DISTRIBUTION = "CoinsDistribution"
    ELECTRONIC_PAYMENTS = "ElectronicPayments"
    INVOICE = "Invoice"
    PERFORMANCE_STATE = "Performancestate"
    ACCOUNTING_MOVEMENTS = "AccountingMovements"
    PAYROLL_STATEMENT_BY_COSTCENTRE = "PayrollStatementByCostcentre"
    WAGE_CHARGE = "WageCharge"
    VACATION_CERTIFICATE = "VacationCertificate"
    CERTIFICATE_FISCAL_EXEMPTION = "CertificateFiscalExemption"
    INDIVIDUAL_ACCOUNT = "IndividualAccount"
    SOCIAL_BALANCE = "SocialBalance"
    PERFORMANCE_STATE_TRANSPORT = "PerformanceStateTransport"
    TOTALS_PER_PAYCODE = "TotalsPerPaycode"
    OVERVIEW_MEAL_VOUCHERS = "OverviewMealVouchers"
    EMPLOYMENT_CERTIFICATE = "EmploymentCertificate"
    EMPLOYEE_SHEET = "EmployeeSheet"
    C103_YOUTH_VACATION = "C103YouthVacation"
    SALARY_SLIP_FISHERMAN = "SalarySlipFisherman"
    LIST_PRESTATIONS_TRANSPORT = "ListPrestationsTransport"
    LIST_NEGATIVE_SALARY_CALCULATIONS = "ListNegativeSalaryCalculations"
    CALCULATE_COST = "CalculateCost"
    LETTER_PAYMENT_EMPLOYER = "LetterPaymentEmployer"
    LIST_CALCULATED_WAGE_ATTACHMENTS = "ListCalculatedWageAttachments"
    C4 = "C4"
    DETAIL_FEES = "DetailFees"
    MISSING_VACATION_CERTIFICATES = "MissingVacationCertificates"
    LIST_OF_COMPANY_CARS = "ListOfCompanyCars"
    C103_SENIOR_VACATION = "C103SeniorVacation"
    C98 = "C98"
    BANKFILE = "Bankfile"
    BELCOTAX_SHEETS = "BelcotaxSheets"
    UNEMPLOYMENT_FUND = "UnemploymentFund"
    NOTICE_TEMP_UNEMPLOYMENT = "NoticeTempUnemployment"
    LETTER_HEADQUARTERS = "LetterHeadquarters"
    C106A_CORONA = "C106ACorona"
    LIST_OPEN_INVOICES = "ListOpenInvoices"
    PAYMENT_REMINDERS = "PaymentReminders"
    LETTER_DOMICILIATION = "LetterDomiciliation"
    REMINDERS_PT = "RemindersPT"
    REMINDERS_NSSO = "RemindersNSSO"
    EXTRA = "Extra"


class LanguageEnum(str, Enum):
    """Language codes supported in Prisma"""
    DUTCH = "N"     # Nederlands
    FRENCH = "F"    # Français
    GERMAN = "D"    # Deutsch
    ENGLISH = "E"   # English


class DocumentModel(BaseModel):
    """Model for document uploads"""
    Filename: str = Field(..., description="The name of the file, with the extension")
    PublicationDate: str = Field(..., min_length=8, max_length=8, pattern=r'^[0-9]*$', description="The date associated with the document in Prisma")
    DocumentType: str = Field(..., description="The document type code defined in Prisma")
    Worker: Optional[int] = Field(None, description="The worker ID as an integer")
    Month: int = Field(..., ge=1, le=12, description="The month the document relates to")
    Year: int = Field(..., description="The year the document relates to")
    Language: Optional[LanguageEnum] = Field(None, description="The language of the document (N=Dutch, F=French, D=German, E=English)")
    Document: str = Field(..., description="Base64 representation of the document's bytestream")
    Size: Optional[int] = Field(None, description="Size of the document in bytes")
    
    # For upload, DocumentID is not needed, but for retrieval it's included
    DocumentID: Optional[int] = Field(None, description="The unique identifier of the document in Prisma")


class DocumentListing(BaseModel):
    """Model for document listing responses"""
    documents: List[DocumentModel] = Field(..., description="List of documents")
